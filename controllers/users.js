//[SECTION] Dependencies and Modules
    const User = require('../models/User');
    const bcrypt = require('bcrypt');
    const Course = require('../models/Course')
    const req = require('express/lib/request');
    const auth = require('../auth');

//[SECTION] Functionalities [Create]
    module.exports.registerUser = (userInput) => {
        let fName = userInput.firstName;
        let lName = userInput.lastName;
        let email = userInput.email;
        let passW = userInput.password;
        let mobil = userInput.mobileNumber;



        let newUser = new User ({
            firstName: fName,
            lastName: lName,
            email: email,
            password: bcrypt.hashSync(passW, 10),
            mobileNo: mobil
        });

        return newUser.save().then((user, err) => {
            if (user) {
                return user
            } else {
                return false;
            }
        })
    }

    //email checker
    module.exports.checkEmailExists = (reqBody) => {
        return User.find({email: reqBody.email}).then(result => {
            if (result.length > 0) {
                return 'Email Already Exists';
            } else {
                return 'Email is still available'
            }
        })
    }
    //Login (User Auth)
    module.exports.loginUser = (reqBody) => {
        let uEmail = reqBody.email;
        let uPassw = reqBody.password;
        return User.findOne({email: uEmail}).then(result => {
            if (result === null) {
                return false
            } else {    
                const isMatched = bcrypt.compareSync(uPassw, result.password)
                if (isMatched) {
                        let userData = result.toObject();
                        return{accessToken: auth.createAccessToken(userData)}
                } else {
                    return false;
                }
            }
        });  
    };

    module.exports.enroll = async (data) => {
        let id = data.userId;
        let course = data.courseId;

        let isUserUpdated = await User.findById(id).then(user => {
            
            let checker = user.enrollments.filter((student) => student.courseId === course);  
                if (checker.length > 0) {
                    return false
                } else {
                    user.enrollments.push({courseId: course});
            
                    return user.save().then((save, error) => {
                        if (error) {
                            
                            return false;
                        } else {
                            return true; 
                        }
                    });
                }

        });

        let isCourseUpdated = await Course.findById(course).then(course => {
            course.enrollees.push({userId: id});

            return course.save().then((saved, err) => {
                if (err) {
                    return false;
                } else {
                    return true; 
                }; 
            });
        }); 

        if (isUserUpdated && isCourseUpdated) {
            return true;
        } else {
            if (!isUserUpdated) {
                return 'User Already Enrolled'
            } else {
                
            }
            return 'Enrollment Failed, Go to Registrar';
        };


     };

//[SECTION] Functionalities [Retrieve]
    
    module.exports.getProfile = (id) => {
     return User.findById(id).then(user => {
        return user; 
     });
   };


//[SECTION] Functionalities [Update]

   module.exports.setAsAdmin = (userId) => {
      let updates = {
        isAdmin: true
      }
      return User.findByIdAndUpdate(userId, updates).then((admin, err) => {
         if (admin) {
            return true; 
         } else {
            return 'Updates Failed to implement'; 
         }
      });
   }; 

   module.exports.setAsNonAdmin = (userId) => {
     let updates = {
        isAdmin: false 
     }
     return User.findByIdAndUpdate(userId, updates).then((user, err) => {  
         if (user) {
            return true; 
         } else {
            return 'Failed to update user'; 
         };
     });
   };


//[SECTION] Functionalities [Delete]