//[SECTION] Dependencies and Moduls
    const exp = require('express');
    const controllerCourses = require('../controllers/courses');
    const auth = require('../auth');
const { decode } = require('jsonwebtoken');

//[SECTION] Routing Component
    const route = exp.Router();

//[SECTION]-[POST] Routes (Admin only)
    route.post('/', auth.verify, (req,res)=> {
        let isAdmin = auth.decode(req.headers.authorization).isAdmin;
        let data = {
            course: req.body,
        }

        if (isAdmin) {
            controllerCourses.addCourse(data).then(outcome => {
                res.send(outcome)
            });  
        } else {
            return res.send('User Not Permitted')
        }
    });

//[SECTION]-[GET] Route
    route.get('/all',auth.verify, (req, res) => {
        let token = req.headers.authorization;
        let payload = auth.decode(token);
        let isAdmin = payload.isAdmin;

        isAdmin ? controllerCourses.getAllCourse().then(outcome => res.send(outcome)) : res.send('UnauthorizedUser')
    });

    route.get('/active', (req, res) => {
        controllerCourses.getActiveCourse().then(outcome => {
            res.send(outcome);
        });
    });

    route.get('/:id', (req, res) => {
        let courseId = req.params.id;
        controllerCourses.getCourse(courseId).then(result => {
            res.send(result);
        });
    });

//[SECTION]-[PUT] Route
    route.put('/:courseId',auth.verify, (req, res) => {
        
        let params = req.params;  
        let body = req.body; 

        if (!auth.decode(req.headers.authorization).isAdmin) {
            res.send('User Unauthorized')
        } else {
            controllerCourses.updateCourse(params, body).then(outcome => {
               res.send(outcome);
            });
        }

  });

  route.put('/:courseId/archive', auth.verify, (req, res) => {
     
     let token = req.headers.authorization;
     let isAdmin = auth.decode(token).isAdmin; 

     let params = req.params; 
     
     (isAdmin) ? 
        controllerCourses.archiveCourse(params).then(result => {
        res.send(result);
        })
       : 
         res.send('Unauthorized User'); 

     
  });

//[SECTION]-[DEL] Route
  route.delete('/:courseId', auth.verify, (req, res)=> {
      let token = req.headers.authorization;
      let payload = auth.decode(token);
      let isAdmin = payload.isAdmin;

      let id = req.params.courseId;

      isAdmin ? controllerCourses.deleteCourse(id).then(outcome => res.send(outcome)) : res.send('Unauthorized User')
  })

//[SECTION] Export Route System
    module.exports = route;